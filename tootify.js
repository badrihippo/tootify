// ==UserScript==
// @name        Tootify
// @namespace   https://badrihippo.github.io
// @version     2
// @description restore the "toot!" button to mastodon
// @author      Badri Sunderarajan
// @match       *://(fosstodon.org|scholar.social)/*
// ==/UserScript==

// By the way, you can add your own Mastodon domain to the "@match" field to make it work there too :)

// This is the actual function that changes the buttons
function tootify() {
  
  // Add some custom CSS
  let tootStyle = document.createElement('style')
  tootStyle.type = 'text/css'
  
  /*
   * Thanks to ZoeBijl on GitHub for the stylesheet:
   *
   * https://gist.github.com/ZoeBijl/581fb6f5575b2ef5b5a5b19906adffaf
   */
  tootStyle.innerHTML = `
    .ui__header__links [href="/publish"] span {
      display:none;
    }

    .compose-form__publish-button-wrapper .button {
      display: flex;
      font-size: 0;
    }

    .compose-form__publish-button-wrapper .button::after,
    .ui__header__links [href="/publish"]::after {
      content: ' Toot!';
      font-size: 1rem;
      display: block;
    }

    /* The button has an exclamation mark ,but the menu item doesn't */
    .ui__header__links [href="/publish"]::after {
      content: ' Toot';
    }
  `

  // Append it to the document head
  document.head.appendChild(tootStyle)
}

// This is the function that activates everything on page load
(function() {
  window.onload = tootify
})()
