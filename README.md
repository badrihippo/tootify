# Tootify

Change the Mastodon "Publish!" buttons back to "Toot!"

## Backstory

It all started with [a toot](https://fosstodon.org/@Cledbetter/109351283152977736).

> Wait they dropped the toot? What will appeal to my inner 5 year old?!?
>
>   —Caroline Ledbetter ([@Cledbetter@fosstodon.org](https://fosstodon.org/@Cledbetter))

The context: Mastodon version 4 was released and brought a lot of changes with it, including responsiveness rules and a completely different mobile interface. The most significant change though, according to some people, was renaming the "Toot!" button to "Publish!".

This is of course better for newcomers because they can easily understand what it means, and one could argue that it's also more formal and professional. But many long-time Mastonauts are used to the "Toot!" button: it's what makes Mastodon *Mastodon!* If you fall in that category (or just support the idea that "Toot!" should be promoted more), this addon will help you bring the old "Toot!" button back!

## Installation

Tootify is a Greasemonkey script, which means it should work in any browser than supports such scripts in any form!

### Get the addon

If you're using a Firefox or Chromium-based browser, you can install any Greasemonkey-compatible extension and add the script to it. This guide will cover using the Tampermonkey extension, though the official Greasemonkey extension should work too.

- For Firefox browsers, [install the extension from the Firefox Addons site](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/).
- For Chromium-based browsers, [install the extension from the Chrome Web Store](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo).

### Adding the script

1. Copy the script from [`tootify.js`](https://git.disroot.org/badrihippo/tootify/src/branch/main/tootify.js) in this repository.
2. Click on the Tampermonkey extension icon and select "Create a new script...".
3. Paste the full text of `tootify.js` into the editor, replacing any text already present.
4. Add your Mastodon domains to the list (see "Modification" below).
5. Save the script.

Now, when you next open your Mastodon, you should see the "Publish!" button changed back to say "Toot!"

## Modification

By default, Tootify runs on the [fosstodon.org](https://fosstodon.org) and [scholar.social](https://scholar.social) instances, where I have my accounts. To add your instance, modify the `@match` setting near the beginning of the script.

Example: here's how the line looks by default:

```
// @match       *://(fosstodon.org|scholar.social)/*
```

As you can see, it has entries for `fosstodon.org` and `scholar.social` within the parentheses, and separated by a vertical bar `|`.

If your account is on [wandering.shop](https://wandering.shop), you could modify it to look like this:

```
// @match       *://(fosstodon.org|scholar.social|wandering.shop)/*
```

Feel free to add as many instances as you want, separated by the `|`!

## Contributing

Contributions are welcome; I don't have any guidelines or anything so just join in! One thing that's on my mind is maybe packaging this as Firefox and Chrome addons so that people who aren't so comfortable doing techy stuff can still have their toot button. Other ideas welcome :)

## License

This script is published under the GNU General Public License version 3 (or later). For the full details, see the [LICENSE](https://git.disroot.org/badrihippo/tootify/src/branch/main/LICENSE) file. If you need a different license feel free to contact me; I like to guarantee my software's freedom by default but I'm open to other uses :)

## Credits

- Original idea from [a Mastodon conversation](https://fosstodon.org/@Cledbetter/109351283152977736) with [@benjaminhollon@fosstodon.org](https://fosstodon.org/@benjaminhollon) and [@Cledbetter@fosstodon.org](https://fosstodon.org/@Cledbetter)
- Installation instructions shamelessly copied from [Umify](https://codeberg.org/benjaminhollon/umify)
- New CSS styles from [this GitHub gist](https://gist.github.com/ZoeBijl/581fb6f5575b2ef5b5a5b19906adffaf) by [@moiety@queer.garden](https://queer.garden/@moiety/109359485653695868)
